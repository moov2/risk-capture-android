package com.risk.data;

import android.provider.BaseColumns;

public class HazardDB implements BaseColumns {

	/*Table*/
	public static final String TABLE_NAME = "hazard";

	/*Fields*/
	public static final String IMAGE_PATH = "imagePath";
	public static final String ASSESSMENT_NAME = "assessmentName";
	public static final String ASSESSMENT_REMOTE_ID = "assessmentRemoteId";
	public static final String TYPE = "type";
	public static final String RATING = "rating";
	public static final String LIKELIHOOD = "likelihood";
	public static final String SEVERITY = "severity";
	public static final String NOTE = "note";
	public static final String THUMBNAIL = "thumbnail";
	public static final String MODIFIED_DATE = "modified";
	public static final String CREATED_DATE = "created";
	public static final String REMOTE_ID = "remoteId";
}
