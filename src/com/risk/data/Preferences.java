package com.risk.data;

public class Preferences {

	public static final String APP_PREFERENCES = "appPreferences";
	
	public static final String CURRENT_ASSESSMENT_NAME = "currentAssessmentName";
	
	public static final String CURRENT_ASSESSMENT_ID = "currentAssessmentID";
	
	public static final String CURRENT_ASSESSMENT_REMOTE_ID = "currentAssessmentRemoteId";
	
	public static final String EDIT_HAZARD_ID = "editHazardId";

	public static final String LIST_ASSESSMENT_ID = "listAssessmentID";	
	
	public static final String MODE = "mode";
	
	public static final String MODE_EDIT = "modeEdit";
	
	public static final String MODE_NEW = "modeNew";

	public static final String USER_EMAIL = "userEmail";

	public static final String EMAIL_ASSESSMENTS = "emailAssessments";
}
