package com.risk.data;

import android.provider.BaseColumns;

public class AssessmentDB implements BaseColumns {

	public static final String CREATED_DATE = "created";
    public static final String MODIFIED_DATE = "modified";

    /*table*/
    public static final String TABLE_NAME= "assessment";
    /*field*/
    public static final String ASSESSMENT_NAME = "assessmentName";
    public static final String ASSESSMENT_ID = "assessmentId";
    public static final String UPLOADED = "uploaded";
    public static final String REMOTE_ID = "remoteId";
}