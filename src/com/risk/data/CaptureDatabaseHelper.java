package com.risk.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CaptureDatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "risk-capture.db";

	private static final int DATABASE_VERSION = 6;

	private static final String TAG = "CaptureDatabaseHelper";

	private static  final String CREATE_ASSESSMENT_TABLE =
		"CREATE TABLE " + AssessmentDB.TABLE_NAME + " ("
		+ AssessmentDB._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
		+ AssessmentDB.CREATED_DATE + " INTEGER,"
		+ AssessmentDB.MODIFIED_DATE + " INTEGER,"
		+ AssessmentDB.ASSESSMENT_NAME + " TEXT,"
		+ AssessmentDB.REMOTE_ID + " TEXT,"
		+ AssessmentDB.UPLOADED + " INTEGER"
		+ ");";
	
	private  static  final  String CREATE_HAZARD_TABLE =
        "CREATE TABLE " + HazardDB.TABLE_NAME + "("
                + HazardDB._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        		+ HazardDB.CREATED_DATE + " INTEGER,"
        		+ HazardDB.MODIFIED_DATE + " INTEGER,"
        		+ HazardDB.REMOTE_ID + " TEXT,"
                + AssessmentDB.ASSESSMENT_ID + " INTEGER,"
                + HazardDB.ASSESSMENT_NAME + " TEXT,"
                + HazardDB.ASSESSMENT_REMOTE_ID + " TEXT,"
                + HazardDB.IMAGE_PATH + " TEXT,"
                + HazardDB.TYPE + " TEXT,"
                + HazardDB.RATING + " INTEGER,"
                + HazardDB.LIKELIHOOD + " INTEGER, "
                + HazardDB.SEVERITY + " INTEGER, "
                + HazardDB.NOTE + " TEXT, "
                + HazardDB.THUMBNAIL + " BLOB"
                + " );";


	public CaptureDatabaseHelper(Context ctx){
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL( CREATE_HAZARD_TABLE );
		
		db.execSQL( CREATE_ASSESSMENT_TABLE );

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + AssessmentDB.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + HazardDB.TABLE_NAME);
		this.onCreate(db);
	}

}
