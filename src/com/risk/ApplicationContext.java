package com.risk;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.risk.data.CaptureDatabaseHelper;

import android.app.Application;
import android.content.Context;

public class ApplicationContext extends Application {

	private static ApplicationContext instance;

	private CaptureDatabaseHelper captureDbHelper;
	
	private HttpClient httpClient;
	
	public ApplicationContext() 
	{
		instance = this;
	}
	
	@Override 
	public void onCreate(){
		
		super.onCreate();
		
		captureDbHelper = this.createDBHelper();
		
		httpClient = this.createHttpClient();
	}
	
	@Override 
	public void onLowMemory()
	{
		super.onLowMemory();
		shutDownHttpClient();
	}
	
	@Override 
	public void onTerminate()
	{
		super.onTerminate();
		this.shutDownHttpClient();
	}

	public static Context getContext() 
	{
		return instance;
	}
	
	public CaptureDatabaseHelper getDBHelper()
	{
		return this.captureDbHelper;
	}

	private CaptureDatabaseHelper createDBHelper()
	{
		 return new CaptureDatabaseHelper( this );
	}
	
	/**
	 * Create a new http client accessible for any Activity in the whole
	 * application
	 */
	private HttpClient createHttpClient()
	{
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
		HttpProtocolParams.setUseExpectContinue(params, true);
		
		SchemeRegistry scheReg = new SchemeRegistry();
		scheReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		scheReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
		ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, scheReg);
		
		return new DefaultHttpClient( conMgr, params );
	}
	
	public HttpClient getHttpClient(){
		return this.httpClient;
	}
	
	private void shutDownHttpClient(){
		if ( this.httpClient !=null && httpClient.getConnectionManager()!=null)
		{
			httpClient.getConnectionManager().shutdown();
		}
		
		if ( this.captureDbHelper != null )
		{
			this.captureDbHelper.close();
			this.captureDbHelper = null;
		}
	}
	
}
