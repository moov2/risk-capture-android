package com.risk.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.risk.R;
import com.risk.activity.base.RiskActivity;
import com.risk.data.Preferences;
import com.risk.data.AssessmentDB;

public class Main extends RiskActivity implements OnItemClickListener {

	public static final String TAG = "RISKCAPTURE:";

	GridView mainGrid;
	Camera camera;
	Button buttonClick;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setRiskContentView(R.layout.main);

		mainGrid = (GridView)findViewById(R.id.mainGrid);
		mainGrid.setAdapter(new com.risk.adapters.MainViewButtonsAdapter(this));

		mainGrid.setOnItemClickListener(this);
		
		//disable scrolling
		mainGrid.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_MOVE){
		            return true;
		        }
				return false;
			}
		});
	}

	/**
	 * Handler for all click events
	 *
	 * @param view
	 */
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		switch( position )
		{
		case 0:
			this.startActivity( new Intent( this, Hazard.class) );
			break;
		case 1:
			showHelp();
			break;
		case 2:
			this.startActivity(new Intent(this, HazardList.class));
			break;
		}		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
			case R.id.menuHelp:
				showHelp();
				return true;
			case R.id.menuSettings:
				startActivity(new Intent(this, RiskSettings.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}