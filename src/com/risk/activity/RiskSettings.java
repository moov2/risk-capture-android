package com.risk.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Window;

import com.risk.R;
import com.risk.activity.base.TitleUtilities;

public class RiskSettings extends PreferenceActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		boolean mTitled = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);		
		addPreferencesFromResource(R.layout.preferences);
		if(mTitled)
			TitleUtilities.setupTitle(this);
	}
}
