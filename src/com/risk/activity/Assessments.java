package com.risk.activity;

import org.bson.types.ObjectId;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.risk.R;
import com.risk.activity.base.RiskActivity;
import com.risk.data.AssessmentDB;
import com.risk.data.Preferences;

public class Assessments extends RiskActivity {
	
	private Assessments _context;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setRiskContentView(R.layout.assessments);
		
		_context = this;
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		if(extras != null)
		{
			extras.getString(Preferences.EDIT_HAZARD_ID);
		}
		
		Button addButton = (Button)findViewById(R.id.addButton);
		addButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				EditText assessmentName = (EditText)findViewById(R.id.assessmentName);
				Editable editableText = assessmentName.getText();
				String newName = editableText.toString();
				
				//create new assessment
				Long assessmentID = _context.createNewAssessment( newName );
				
				startHazardActivity( assessmentID );				
			}
		});
	}
	
	@Override
	public void onStart(){
		super.onStart();
		setUpAssessmentList();
	}
	
	@Override
	public void onStop(){
		super.onStop();
		stopList();
	}
	
	/**
	 * Start the Hazard Activity
	 */
	public void startHazardActivity( Long id )
	{
		Intent hazardIntent = (new Intent( this, Hazard.class));
		hazardIntent.putExtra(Preferences.CURRENT_ASSESSMENT_ID, id );
		setResult(RESULT_OK, hazardIntent);
		finish();
	}
	
	private Cursor _cursor;
	private ListAdapter _adapter;
	
	/**
	 * Creates a list of assessment to select from
	 */
	private void setUpAssessmentList()
	{
		SQLiteDatabase db = this.getDatabase();
		_cursor =  db.query(AssessmentDB.TABLE_NAME, null, null, null, null, null, null);
		
		this.startManagingCursor(_cursor);
		
		String[] columns = new String[]{ AssessmentDB.ASSESSMENT_NAME };
		int[] view = new int[]{ R.id.assessmentName};
		
		_adapter = new SimpleCursorAdapter(this, R.layout.assessment_item_renderer, _cursor, columns, view);
		
		ListView assessmentList = (ListView)findViewById(R.id.assessmentlistView);
		assessmentList.setAdapter(_adapter);
		
		assessmentList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) 
			{
				String name = ((TextView)v).getText().toString();
				
				Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
				
				selectAssessmentByNameAndReturnToHazard(name);
			}
		});
		
		assessmentList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View v,
					int arg, long id) {
				final String assessmentName = ((TextView)v).getText().toString();
				final int elementIndex = parent.getPositionForView(v);
				final AdapterView<?> parentRef = parent;
				String selectAssessment = getResources().getText(R.string.dialogSelect) + " " + assessmentName;
				String deleteAssessment = getResources().getText(R.string.dialogDelete) + " " + assessmentName;
				final AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext()); 
				AlertDialog options = builder.setItems(new CharSequence[]{ selectAssessment, deleteAssessment }, 
						new DialogInterface.OnClickListener() {					
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0://select
							selectAssessmentByNameAndReturnToHazard(assessmentName);
							break;
						case 1://delete
							new AlertDialog.Builder(_context).setTitle(R.string.dialogDelete)
							.setMessage(R.string.dialogConfirmDeleteAssessmentAndHazards)
							.setPositiveButton(R.string.dialogYes, new DialogInterface.OnClickListener() {								
								public void onClick(DialogInterface dialog, int which) {
									deleteAssessmentByName(assessmentName);
									parentRef.removeViews(elementIndex, 1);
								}
							}).setNegativeButton(R.string.dialogNo, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) { 
									//Do nothing
								}
							}).show();
							break;
						default:
							break;
						}
					}
				}).create();
				options.show();
				return true;
			}
		});
		
		db.close();
	}
	
	public void deleteAssessmentByName(String assessmentName){
		SQLiteDatabase db = getDatabase();
		Cursor cursor = db.query(AssessmentDB.TABLE_NAME, new String[]{ AssessmentDB._ID } , AssessmentDB.ASSESSMENT_NAME + " = ?", new String[]{assessmentName}, null, null, null);
		if(cursor.getCount() > 0){
			cursor.moveToFirst();
			String assessmentId = cursor.getString(cursor.getColumnIndex(AssessmentDB._ID));
			db.delete(com.risk.data.HazardDB.TABLE_NAME, AssessmentDB.ASSESSMENT_ID + " = ?", new String[]{ assessmentId });
			db.delete(AssessmentDB.TABLE_NAME, AssessmentDB._ID + " = ?", new String[]{ assessmentId });
		}else{
			db.delete(com.risk.data.HazardDB.TABLE_NAME, com.risk.data.HazardDB.ASSESSMENT_NAME + " = ?", new String[]{ assessmentName });
		}
		cursor.close();
		db.close();
	}
	
	public void selectAssessmentByNameAndReturnToHazard(String assessmentName){
		SQLiteDatabase db = this.getDatabase();
		String[] args = new String[]{ assessmentName };
		String[] cols = new String[] { AssessmentDB._ID, AssessmentDB.REMOTE_ID };
		Cursor cursor = db.query(AssessmentDB.TABLE_NAME, cols, AssessmentDB.ASSESSMENT_NAME+" = ?", args, null, null, null);
		cursor.moveToFirst();	
		
		long assessmentId = cursor.getLong(cursor.getColumnIndex(AssessmentDB._ID));
		
		cursor.close();
		db.close();
		
		_context.setActiveAssessmentId(assessmentId);
		startHazardActivity(assessmentId);
	}
	
	private void stopList(){
		this.stopManagingCursor(_cursor);
		_adapter = null;
		if(!_cursor.isClosed())
			_cursor.close();
		_cursor = null;
	}
	
	/**
	 * Create a new Assessment
	 * 
	 */
	private long createNewAssessment( String name)
	{
		SQLiteDatabase db = this.getDatabase();

		Long now = Long.valueOf(System.currentTimeMillis());

		ContentValues values = new ContentValues();
		String remoteId = ObjectId.get().toString();
		values.put(AssessmentDB.REMOTE_ID, remoteId);
		values.put(AssessmentDB.CREATED_DATE, now);
		values.put(AssessmentDB.MODIFIED_DATE, now);
		values.put(AssessmentDB.ASSESSMENT_NAME, name);
		values.put(AssessmentDB.UPLOADED, 0);
		
		Long id = db.insertOrThrow( AssessmentDB.TABLE_NAME, null, values );
		db.close();
		
		setActiveAssessmentId(id);

		return id;
	}
}
