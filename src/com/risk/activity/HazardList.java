package com.risk.activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.risk.ApplicationContext;
import com.risk.R;
import com.risk.activity.base.ListRiskActivity;
import com.risk.data.AssessmentDB;
import com.risk.data.HazardDB;
import com.risk.data.Preferences;
import com.risk.remote.RiskUploader;

public class HazardList extends ListRiskActivity {
	
	private long _listAssessmentId = -1;
	private SQLiteDatabase _db;
	private Cursor _cursor;
	private SimpleCursorAdapter _adapter;
	private ArrayList<HashMap<String,String>> _assessments;
	private ProgressDialog _pd;
	private HazardList _context = this;
	
	private static final String LIST_ASSESSMENT_ID = "listAssessmentId";
	
	@Override
	public void onCreate( Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		if(savedInstanceState != null){
			if(savedInstanceState.containsKey(LIST_ASSESSMENT_ID))
			_listAssessmentId = savedInstanceState.getLong(LIST_ASSESSMENT_ID);			
		}
		
		setRiskContentView(R.layout.hazard_list);
		
		Button selectAssessment = (Button)findViewById(R.id.assessmentSelect);
		selectAssessment.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				AlertDialog.Builder builder =  new AlertDialog.Builder(v.getContext());
				builder.setTitle(R.string.select_assessment)
				       .setItems(getAssessmentNames(), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							HashMap<String, String> assessment = _assessments.get(which);
							long assessmentId = Long.parseLong(assessment.get(AssessmentDB._ID));
							String assessmentName = (String)assessment.get(AssessmentDB.ASSESSMENT_NAME);
							setActiveAssessment(assessmentId, assessmentName);
						}
						
					});
				builder.create().show();
			}
		});		
		
		Button syncAssessment = (Button)findViewById(R.id.assessmentMenu);
		syncAssessment.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				Resources res = getResources();
				if(_listAssessmentId != -1){
					new AlertDialog.Builder(v.getContext())
					.setItems(new CharSequence[]{ res.getString(R.string.menuNewHazard), res.getString(R.string.menuSync), res.getString(R.string.menuDeleteAssessment), res.getString(R.string.menuHelp) }, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
								case 0:
									newHazardCurrentAssessment();
									break;
								case 1:
									syncCurrentAssessment();
									break;
								case 2:
									deleteCurrentAssessmentWithConfirmation();
									break;
								case 3:
									showHelp();
									break;								
								default:
									break;
							}
						}
					}).show();
				}else{	
					new AlertDialog.Builder(v.getContext())
					.setItems(new CharSequence[]{ res.getString(R.string.menuNewHazard), res.getString(R.string.menuHelp) }, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								newHazard();
								break;
							case 1:
								showHelp();
								break;
							default:
								break;
							}
						}
					}).show();
				}
			}
		});
	}
	
	private void newHazard(){
		startActivity(new Intent(this, Hazard.class));
	}
	
	private void newHazardCurrentAssessment() {
		if(_listAssessmentId != -1)
			setActiveAssessmentId(_listAssessmentId);
		newHazard();
	}
	
	private void setActiveAssessment(long assessmentId){
		for(HashMap<String, String> assessment : _assessments){
			try{
			if((Long.parseLong(assessment.get(AssessmentDB._ID)))==assessmentId){
				setActiveAssessment(assessmentId, (String)assessment.get(AssessmentDB.ASSESSMENT_NAME));
				break;
			}
			}catch(NumberFormatException e){
				
			}
		}
	}
	
	private void setActiveAssessment(long assessmentId, String assessmentName){
		_listAssessmentId = assessmentId;
		Button button = (Button)findViewById(R.id.assessmentSelect);
		button.setText((String)assessmentName);
		closeList();
		setupList();
	}
	
	private void promptForEmailAndUploadOnSuccessfulEntry(Context context){
		final EditText input = new EditText(context);
		new AlertDialog.Builder(context)
	    .setTitle(R.string.dialogEnterEmail)
	    .setMessage(R.string.dialogEnterEmailSyncMessage)
	    .setView(input)
	    .setPositiveButton(R.string.dialogSave, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            String email = input.getText().toString();
	            saveUserEmail(email);
	            uploadAssessment();
	        }
	    }).setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            //Do nothing
	        }
	    }).show();
	}
	
	private void syncCurrentAssessment(){
		if(_listAssessmentId != -1){
			if(connectionIsAvailable()){
				if(!getUserEmail().equals("")){
					uploadAssessment();
				}else{
					promptForEmailAndUploadOnSuccessfulEntry(this);
				}						
			}else{
				Toast.makeText(getApplicationContext(), R.string.connection_unavailable, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void uploadAssessment(){
		try {
			RiskUploader uploadTask = new RiskUploader(_db, getUserEmail(), getSendAssessmentEmails()){
				@Override
				protected void onPreExecute(){
					_pd = new ProgressDialog(_context);
					_pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					_pd.setTitle(R.string.dialogUploadingAssessment);
					_pd.setMax(100);
					_pd.show();
				}
				
				@Override
				protected void onProgressUpdate(
						Integer... values) {
					super.onProgressUpdate(values);
					_pd.setProgress(values[0]);
				}
				
				@Override
				protected void onPostExecute(Integer result) {
					super.onPostExecute(result);
					_pd.dismiss();
				};
			};
			uploadTask.execute((int)_listAssessmentId);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		setupList();
		setupAssessments();		
	}
	
	private void setupAssessments() {
		if(_db==null){
			ApplicationContext app = (ApplicationContext)this.getApplication();
			_db = app.getDBHelper().getReadableDatabase();
		}
		Cursor cursor = _db.query(AssessmentDB.TABLE_NAME, null, null, null, null, null, null);
		_assessments = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> all = new HashMap<String, String>();
		all.put(AssessmentDB._ID, String.valueOf((long)-1));
		all.put(AssessmentDB.ASSESSMENT_NAME, "All");
		all.put(AssessmentDB.REMOTE_ID, "");
		_assessments.add(all);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			HashMap<String, String> assessment = new HashMap<String, String>();
			long assessmentId = cursor.getLong(cursor.getColumnIndex(AssessmentDB._ID));
			String assessmentName = cursor.getString(cursor.getColumnIndex(AssessmentDB.ASSESSMENT_NAME));
			if(cursor.getString(cursor.getColumnIndex(AssessmentDB._ID)).equals(_listAssessmentId))
				setActiveAssessment(assessmentId, assessmentName);
			assessment.put(AssessmentDB._ID, String.valueOf(assessmentId));
			assessment.put(AssessmentDB.ASSESSMENT_NAME, assessmentName);
			assessment.put(AssessmentDB.REMOTE_ID, cursor.getString(cursor.getColumnIndex(AssessmentDB.REMOTE_ID)));
			_assessments.add(assessment);
			cursor.moveToNext();
		}
		cursor.close();
	}
	
	private CharSequence[] getAssessmentNames(){
		
		if(_assessments!=null){
			CharSequence[] output = new CharSequence[_assessments.size()];
			int i = 0;
			for(HashMap<String, String> assessment : _assessments){
				output[i] = (CharSequence)assessment.get(AssessmentDB.ASSESSMENT_NAME);
				i++;
			}
			return output;
		}else{
			return new CharSequence[]{};
		}
	}

	private void setupList(){		
		if(_db==null){
			ApplicationContext app = (ApplicationContext)this.getApplication();
			_db = app.getDBHelper().getReadableDatabase();
		}
			
		if(_listAssessmentId != -1){
			String[] args = new String[]{ String.valueOf(_listAssessmentId) };
			_cursor = _db.query(HazardDB.TABLE_NAME, null, AssessmentDB.ASSESSMENT_ID + "= ? ", args, null, null, HazardDB.MODIFIED_DATE + " DESC");
		}else
			_cursor = _db.query(HazardDB.TABLE_NAME, null, null, null, null, null, HazardDB.MODIFIED_DATE + " DESC");
		this.startManagingCursor(_cursor);
		
		String[] from = new String[]{HazardDB._ID, HazardDB.ASSESSMENT_NAME, HazardDB.NOTE, HazardDB.THUMBNAIL }; 
		int[] to = new int[]{ R.id.hazardId, R.id.assessmentName, R.id.hazardNotes, R.id.hazardImage };
		
		// Two different ways to accomplish using blob data in listview:
		
		// Approach 1: custom adapter (see hazardlistitemadapter class for details)
		//HazardListItemAdapter adapter = new HazardListItemAdapter(this, R.layout.hazard_item_renderer, cursor, from, to);
		
		// Approach 2: custom ViewBinder
		SimpleCursorAdapter.ViewBinder viewBinder = new SimpleCursorAdapter.ViewBinder() {
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
		       
				if (columnIndex == cursor.getColumnIndex(HazardDB.THUMBNAIL))
				{
					ImageView iv = (ImageView)view.findViewById(R.id.hazardImage);
			        byte[] thumbnail = cursor.getBlob(columnIndex);
			        
			        if(thumbnail != null)
			        	iv.setImageBitmap(BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length));
			        else
			        	iv.setImageResource(android.R.color.transparent);
				}
				else
				{
					return false;
				}
				return true;
			}
		};
		
		_adapter = new SimpleCursorAdapter(this, R.layout.hazard_item_renderer, _cursor, from, to);
		_adapter.setViewBinder(viewBinder);
		
		// the 2nd approach is less code but the first possibly allows greater flexibility...
		// see this post for more details: http://stackoverflow.com/questions/5573539/android-using-simplecursoradapter-to-set-colour-not-just-strings
		
		this.setListAdapter(_adapter);
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		closeList();
	}
	
	private void closeList(){
		this.stopManagingCursor(_cursor);
		this.setListAdapter(null);
		if(_adapter != null){
			_adapter.getCursor().close();
			_adapter = null;
		}
		if(_cursor!=null){
			_cursor.close();
			_cursor = null;
		}
		if(_db!=null){			
			_db.close();
			_db = null;
		}
	}
	
	private void deleteCurrentAssessmentWithConfirmation(){
		new AlertDialog.Builder(this)
		.setTitle(R.string.dialogDelete)
		.setMessage(getResources().getString(R.string.dialogConfirmDeleteAssessmentAndHazards))
		.setPositiveButton(R.string.dialogDelete, new  DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				deleteAssessment(_listAssessmentId);				
			}
		})
		.setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// Do Nothing
			}
		}).show();
	}
	
	@Override
	public void deleteAssessment(long assessmentId){
		super.deleteAssessment(assessmentId);
		if(assessmentId != -1){
			for(int i = 0;i<_assessments.size();i++){
				if(((String)_assessments.get(i).get(AssessmentDB._ID)).equals(assessmentId)){
					_assessments.remove(i);
					break;
				}
			}
		}
		setActiveAssessment(-1);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.hazard_list_menu, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu){
		MenuItem sync = menu.findItem(R.id.menuSync);
		MenuItem delete = menu.findItem(R.id.menuDelete);
		boolean enabled = (_listAssessmentId != -1);
		sync.setVisible(enabled);
		sync.setEnabled(enabled);
		delete.setVisible(enabled);
		delete.setEnabled(enabled);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()) {
		case R.id.menuNewHazard:
			newHazardCurrentAssessment();
			return true;
		case R.id.menuSync:
			syncCurrentAssessment();
			return true;
		case R.id.menuHelp:
			showHelp();
			return true;
		case R.id.menuDelete:
			deleteCurrentAssessmentWithConfirmation();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		TextView idTextView = (TextView)v.findViewById(R.id.hazardId);
		long hazardId = -1;
		try{
			hazardId = Long.parseLong(idTextView.getText().toString());			
		}catch(NumberFormatException e){
			
		}				
		Intent hazardIntent = new Intent( this, Hazard.class );
		hazardIntent.putExtra(Preferences.EDIT_HAZARD_ID, hazardId);
		this.startActivity( hazardIntent );
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		super.onSaveInstanceState(outState);
		outState.putLong(LIST_ASSESSMENT_ID, _listAssessmentId);
	}
	
}
