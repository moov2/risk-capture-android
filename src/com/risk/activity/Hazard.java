package com.risk.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;

import org.bson.types.ObjectId;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.risk.R;
import com.risk.activity.base.TextRiskActivity;
import com.risk.data.AssessmentDB;
import com.risk.data.HazardDB;
import com.risk.data.Preferences;
import com.risk.util.BitmapUtil;

public class Hazard extends TextRiskActivity {

	private static final String ASSESSMENT_ID = "assessmentID";
	private static int TAKE_PICTURE = 1;
	private static int CHANGE_ASSESSMENT = 2;
	
	private static int THUMBNAIL_WIDTH = 100;
	private static int THUMBNAIL_HEIGHT = 100;

	Camera camera;
	Button buttonClick;
	Bitmap bitmap;
	private long _assessmentID;
	private String _asssessmentName;
	private String _asssessmentRemoteId;
	private static Uri _imageUri;
	private Bitmap _thumbnailBitmap;
	private long _editHazardId;
	private boolean _newHazard;
	
	private static EditText[] sFields;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setRiskContentView(R.layout.hazard);
		
		Intent intent = getIntent();
	    String action = intent.getAction();
	    String type = intent.getType();		 

		if (Intent.ACTION_SEND.equals(action) && type != null) {
	        if (type.startsWith("image/")) {
	            handleSendImage(intent); // Handle single image being sent
	            _newHazard = true;
	            _assessmentID = getActiveAssessmentId();
	        }
	    } else {
	    	Bundle extras = intent.getExtras();
	    	if(extras != null)
			{
	    		_newHazard = !extras.containsKey(Preferences.EDIT_HAZARD_ID);
	    		if(!_newHazard)
	    			_editHazardId = extras.getLong(Preferences.EDIT_HAZARD_ID, -1);
	    		if(_editHazardId == -1)
	    			_newHazard = true;
				
			}else{
				_newHazard = true;
			}
	    }

		this.initEventListeners();
		
		if(_newHazard)
			this.loadCurrentAssessmentDetails();
		else
			this.loadExistingHazard();
		
		if(_assessmentID == -1)//No assessments exist yet
			selectAssessment();
	}
	
	private void handleSendImage(Intent intent) {
		Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
	    if (imageUri != null) {
	    	_imageUri = imageUri;
	    	processImageFromImageUri();
	    }
	}

	@Override
	public void onStart(){
		super.onStart();
		EditText notes = (EditText)findViewById(R.id.hazardNotes);
		sFields = new EditText[] {notes};
	}

	@Override
	protected EditText[] getFields() {
		return sFields;
	}
	
	private void loadCurrentAssessmentDetails(){
		_assessmentID = getActiveAssessmentId();
		loadAssessmentDetails(_assessmentID);
	}
	
	private void loadAssessmentDetails(long assessmentId){
		SQLiteDatabase db = this.getDatabase();
		
		String[] columns = new String[]{AssessmentDB.ASSESSMENT_NAME, AssessmentDB.REMOTE_ID};
		String[] args = new String[]{ String.valueOf(assessmentId) };
		Cursor cursor = db.query(AssessmentDB.TABLE_NAME, columns, AssessmentDB._ID + " = ?", args, null, null, null);
		if(cursor.getCount() > 0){
			cursor.moveToFirst();
			setAssessmentDetails(assessmentId, cursor.getString(cursor.getColumnIndex(AssessmentDB.ASSESSMENT_NAME)), cursor.getString(cursor.getColumnIndex(AssessmentDB.REMOTE_ID)));			
		}
		cursor.close();
		db.close();
	}
	
	private void setAssessmentDetails(long assessmentId, String assessmentName, String assessmentRemoteId){
		_assessmentID = assessmentId;
		_asssessmentName = assessmentName;
		_asssessmentRemoteId = assessmentRemoteId;
		TextView tvAssessmentName = (TextView)findViewById(R.id.assessmentName);
		tvAssessmentName.setText(this._asssessmentName);
	}
	
	private void loadExistingHazard() {
		SQLiteDatabase db = this.getDatabase();
		
		String[] columns = new String[]{AssessmentDB.ASSESSMENT_ID, HazardDB.ASSESSMENT_NAME, HazardDB.ASSESSMENT_REMOTE_ID, HazardDB.THUMBNAIL, HazardDB.TYPE, HazardDB.NOTE, HazardDB.LIKELIHOOD, HazardDB.SEVERITY, HazardDB.IMAGE_PATH};
		String[] args = new String[]{ String.valueOf(_editHazardId) };
		Cursor cursor = db.query(HazardDB.TABLE_NAME, columns, HazardDB._ID + " = ?", args, null, null, null);
		cursor.moveToFirst();
		
		_imageUri = new Uri.Builder().path(cursor.getString(cursor.getColumnIndex(HazardDB.IMAGE_PATH))).build();
		
		// set assessment name
		_asssessmentName = cursor.getString(cursor.getColumnIndex(HazardDB.ASSESSMENT_NAME));
		TextView assessmentName = (TextView)findViewById(R.id.assessmentName);
		assessmentName.setText(_asssessmentName);
		
		// set thumbnail
		ImageView iv = (ImageView) findViewById(R.id.imageView);
        byte[] thumbnail = cursor.getBlob(cursor.getColumnIndex(HazardDB.THUMBNAIL));
        
        if(thumbnail != null)
        	iv.setImageBitmap(BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length));
		        
        long savedAssessmentId = cursor.getLong(cursor.getColumnIndex(AssessmentDB.ASSESSMENT_ID));
        if(_assessmentID != -1 && savedAssessmentId != _assessmentID){
        	loadCurrentAssessmentDetails();
        }else{
        	if(_assessmentID==-1){
        		_assessmentID = savedAssessmentId;
        	}
	        _asssessmentName = cursor.getString(cursor.getColumnIndex(HazardDB.ASSESSMENT_NAME));
			_asssessmentRemoteId = cursor.getString(cursor.getColumnIndex(HazardDB.ASSESSMENT_REMOTE_ID));
        }
        
		// set notes
		EditText notes = (EditText)findViewById(R.id.hazardNotes);
		notes.setText(cursor.getString(cursor.getColumnIndex(HazardDB.NOTE)));
		
		Spinner spinner = (Spinner)findViewById(R.id.hazardTypeSpinner);
		spinner.setSelection(cursor.getInt(cursor.getColumnIndex(HazardDB.TYPE)));
		
		spinner = (Spinner)findViewById(R.id.likelihoodSpinner);
		spinner.setSelection(cursor.getInt(cursor.getColumnIndex(HazardDB.LIKELIHOOD)));
		
		spinner = (Spinner)findViewById(R.id.severitySpinner);
		spinner.setSelection(cursor.getInt(cursor.getColumnIndex(HazardDB.SEVERITY)));
		
		cursor.close();
		db.close();
	}

	/**
	 * @private
	 * Initialise Events listeners for components
	 */
	private void initEventListeners()
	{
		setUpSpinner(R.id.hazardTypeSpinner, R.array.hazardTypes);
		
		setUpSpinner(R.id.likelihoodSpinner, R.array.likelihoodSelections);
		
		setUpSpinner(R.id.severitySpinner, R.array.severitySelections);

		TextView newAssessment = (TextView)findViewById(R.id.assessmentName);
		newAssessment.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				selectAssessment();
			}
		});

		ImageButton button = (ImageButton)findViewById(R.id.captureButton);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				takePhoto(v);
			}
		});

		Button saveButton = (Button)findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				saveHazard();
			}
		});
	}
	
	private void selectAssessment(){
		Intent assessmentIntent = new Intent(this, Assessments.class);
		startActivityForResult(assessmentIntent, CHANGE_ASSESSMENT);
	}
	
	/**
	 * @private
	 * Set up a Spinner
	 */
	private void setUpSpinner( int viewId, int arrayId )
	{
		Spinner typeSpinner = (Spinner)findViewById( viewId );
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, arrayId,
				android.R.layout.simple_spinner_item);
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		typeSpinner.setAdapter(adapter);
	}

	public void takePhoto(View view) {

		File path = new File(Environment.getExternalStorageDirectory().getPath() + "/RiskCapture/");
		path.mkdirs();
		File file = new File(path, System.currentTimeMillis() + ".jpg");
		_imageUri = Uri.fromFile(file);
		
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT,_imageUri);
		startActivityForResult( intent, TAKE_PICTURE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode != Activity.RESULT_OK || (requestCode != TAKE_PICTURE && requestCode != CHANGE_ASSESSMENT)){
			return;
		}
		
		if(requestCode == TAKE_PICTURE)
			processImageFromImageUri();
		
		if(requestCode == CHANGE_ASSESSMENT){
			Bundle extras = data.getExtras();
			long assessmentId = extras.getLong(Preferences.CURRENT_ASSESSMENT_ID, -1);
			loadAssessmentDetails(assessmentId);
		}			
	}

	private void processImageFromImageUri(){
		// create thumbnail from saved image and set to imageview
		ImageView imageView = (ImageView) findViewById(R.id.imageView);
		Bitmap fullSizeBmp  = BitmapUtil.GetFromMediaStore(getContentResolver(), _imageUri);
		_thumbnailBitmap = BitmapUtil.Create(fullSizeBmp, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
		
		int padding = (imageView.getWidth() - fullSizeBmp.getWidth())/2;
		imageView.setPadding(padding, 0, padding, 0);
		
		imageView.setImageBitmap(_thumbnailBitmap);
	}
	
	private String getFilePathFromUri(Uri uri) {
		  Cursor cursor = managedQuery(uri,
		                               new String[] { MediaStore.Images.Media.DATA },
		                               null, null, null);
		  int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		  cursor.moveToFirst();

		  return cursor.getString(column_index);
		}

	/**
	 * Save a hazard picture
	 * 
	 */
	private void saveHazard(){

		SQLiteDatabase db = this.getDatabase();
		
		EditText notes = (EditText)findViewById(R.id.hazardNotes);
		
		ContentValues hazardValues = new ContentValues();
		hazardValues.put(AssessmentDB.ASSESSMENT_ID, _assessmentID);
		
		Long now = Long.valueOf(System.currentTimeMillis());
		

		String imagePath = "";
		if(_imageUri!=null){
			if("content".equals(_imageUri.getScheme()))
				imagePath = getFilePathFromUri(_imageUri);
			else
				imagePath = _imageUri.getPath();
		}
		hazardValues.put(HazardDB.IMAGE_PATH, imagePath);
		hazardValues.put(HazardDB.ASSESSMENT_NAME, _asssessmentName);
		hazardValues.put(HazardDB.ASSESSMENT_REMOTE_ID, _asssessmentRemoteId);
		hazardValues.put(HazardDB.TYPE, getSpinnerValue(R.id.hazardTypeSpinner));
		hazardValues.put(HazardDB.SEVERITY, getSpinnerValue(R.id.severitySpinner));
		hazardValues.put(HazardDB.LIKELIHOOD, getSpinnerValue(R.id.likelihoodSpinner));
		hazardValues.put(HazardDB.RATING, 1);
		hazardValues.put(HazardDB.NOTE, notes.getText().toString() );
		hazardValues.put(HazardDB.MODIFIED_DATE, now);
		
		if(_thumbnailBitmap != null)
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			_thumbnailBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
			byte[] thumbnailBlob = baos.toByteArray();
			hazardValues.put(HazardDB.THUMBNAIL, thumbnailBlob);
		}		
		if(!_newHazard)			
			db.update(HazardDB.TABLE_NAME, hazardValues, HazardDB._ID + " = ?", new String[]{String.valueOf(_editHazardId) });
		else{
			hazardValues.put(HazardDB.REMOTE_ID, ObjectId.get().toString());
			hazardValues.put(HazardDB.CREATED_DATE, now);
			db.insertOrThrow(HazardDB.TABLE_NAME, null, hazardValues);
		}
		
		ContentValues assessmentValues = new ContentValues();
		
		assessmentValues.put(AssessmentDB.MODIFIED_DATE, now);
		
		db.update(AssessmentDB.TABLE_NAME, assessmentValues, AssessmentDB._ID + " = ?", new String[]{ String.valueOf(_assessmentID) });

		Intent homeViewIntent = new Intent( this, Main.class );
		homeViewIntent.putExtra(ASSESSMENT_ID, this._assessmentID);
		
		db.close();

		setResult(RESULT_OK);
		finish();
	}
	
	/**
	 * Gets the spinners selected text
	 * 
	 * @param id
	 * @return String
	 */
	private String getSpinnerValue( int id)
	{
		Spinner spinner = (Spinner)findViewById(id);
		
		return Long.toString( spinner.getSelectedItemId() );
	}
}
