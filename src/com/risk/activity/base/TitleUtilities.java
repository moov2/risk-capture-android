package com.risk.activity.base;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.risk.R;
import com.risk.activity.Main;

public class TitleUtilities {
	public static void setupTitle(Activity context){
		context.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.main_title_bar);
		Button titleButton = (Button)context.findViewById(R.id.title_logo_button);
		titleButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				v.getContext().startActivity(new Intent(v.getContext(), Main.class));
			}
		});
	}
}
