package com.risk.activity.base;

import com.risk.ApplicationContext;
import com.risk.data.AssessmentDB;
import com.risk.util.Connection;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.view.Window;

public abstract class ListRiskActivity extends ListActivity implements SharedRiskMethods {
	
	private boolean mTitled;
	
	public void setRiskContentView(int resource){
		mTitled = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(resource);
		if(mTitled)
			TitleUtilities.setupTitle(this);
	}

	public void saveUserEmail(String email) {
		PreferencesUtilities.setUserEmail(this, email);
	}

	public String getUserEmail() {
		return PreferencesUtilities.getUserEmail(this);
	}
	
	public boolean getSendAssessmentEmails(){
		return PreferencesUtilities.getSendEmailAssessments(this);
	}
	
	public void saveSendAssessmentEmails(boolean sendEmails){
		PreferencesUtilities.setSendEmailAssessments(this, sendEmails);
	}

	public boolean connectionIsAvailable() {
		return Connection.Available(this);
	}
	
	public void deleteAssessment(String assessmentId){
		try{
			deleteAssessment(Long.parseLong(assessmentId));
		}catch(NumberFormatException e){
			
		}
	}
	
	public void deleteAssessment(long assessmentId){
		SQLiteDatabase db = getDatabase();
		String idString = String.valueOf(assessmentId);
		db.delete(com.risk.data.HazardDB.TABLE_NAME, AssessmentDB.ASSESSMENT_ID + " = ?", new String[]{ idString });
		db.delete(AssessmentDB.TABLE_NAME, AssessmentDB._ID + " = ?", new String[]{ idString });
		db.close();
	}
	
	public void showHelp(){
		Intent helpIntent = new Intent( Intent.ACTION_VIEW);
		helpIntent.setData(Uri.parse("http://www.lightinteractive.co.uk/examples/androidrisk/help.htm"));
		this.startActivity(helpIntent);
	}
	
	public void setActiveAssessmentId(long assessmentId){
		PreferencesUtilities.setActiveAssessmentId(this, assessmentId);
	}
	
	public long getActiveAssessmentId(){
		long assessmentId = PreferencesUtilities.getActiveAssessmentId(this);
		if(assessmentExists(assessmentId))
			return assessmentId;
		else
			return -1;
	}
	
	public boolean assessmentExists(long assessmentId){
		SQLiteDatabase db = getDatabase();
		Cursor cursor = db.query(AssessmentDB.TABLE_NAME, new String[]{ AssessmentDB._ID }, AssessmentDB._ID + " = ?", new String[]{String.valueOf(assessmentId)}, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count > 0;
	}
	
	/**
	 * @private
	 * 
	 * @return SQLiteDatabase
	 */
	public SQLiteDatabase getDatabase(){

		ApplicationContext app = (ApplicationContext)this.getApplication();
		return app.getDBHelper().getWritableDatabase();
	}
}
