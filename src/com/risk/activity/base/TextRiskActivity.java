package com.risk.activity.base;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public abstract class TextRiskActivity extends RiskActivity {
	
	private long _touchDownTime;
	
	/**
	 * Called to process touch screen events.
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {		
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			_touchDownTime = SystemClock.elapsedRealtime();
			break;

		case MotionEvent.ACTION_UP:
			// to avoid drag events
			if (SystemClock.elapsedRealtime() - _touchDownTime <= 150) {

				EditText[] textFields = this.getFields();
				if (textFields != null && textFields.length > 0) {

					boolean clickIsOutsideEditTexts = true;

					for (EditText field : textFields) {
						if (isPointInsideView(ev.getRawX(), ev.getRawY(), field)) {
							clickIsOutsideEditTexts = false;
							break;
						}
					}

					if (clickIsOutsideEditTexts) {
						this.hideSoftKeyboard();
					}
				} else {
					this.hideSoftKeyboard();
				}
			}
			break;
		}

		return super.dispatchTouchEvent(ev);
	}

	protected abstract EditText[] getFields();

	private void hideSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}

	private boolean isPointInsideView(float rawX, float rawY, EditText field) {
		return (field.getLeft() < rawX && field.getRight() > rawX && field.getTop() < rawY && field.getBottom() > rawY);
	}

}
