package com.risk.activity.base;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.risk.data.Preferences;

public class PreferencesUtilities {
	public static String getUserEmail(Activity context){
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		return appPreferences.getString(Preferences.USER_EMAIL, "");
	}
	
	public static void setUserEmail(Activity context, String email){
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = appPreferences.edit();
		editor.putString(Preferences.USER_EMAIL, email);
		editor.commit();
	}
	
	public static boolean getSendEmailAssessments(Activity context){
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		return appPreferences.getBoolean(Preferences.EMAIL_ASSESSMENTS, true);
	}
	
	public static void setSendEmailAssessments(Activity context, boolean sendEmails){
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = appPreferences.edit();
		editor.putBoolean(Preferences.EMAIL_ASSESSMENTS, sendEmails);
		editor.commit();
	}

	public static void setActiveAssessmentId(Activity context, long assessmentId) {		
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = appPreferences.edit();
		editor.putLong(Preferences.CURRENT_ASSESSMENT_ID, assessmentId);
		editor.commit();
	}

	public static long getActiveAssessmentId(Activity context) {
		SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		return appPreferences.getLong(Preferences.CURRENT_ASSESSMENT_ID, -1);
	}
}
