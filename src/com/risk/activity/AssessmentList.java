package com.risk.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.risk.ApplicationContext;
import com.risk.R;
import com.risk.data.AssessmentDB;
import com.risk.data.Preferences;

public class AssessmentList extends ListActivity {
	@Override
	public void onCreate( Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		this.setContentView(R.layout.assessment_list);
		
		ApplicationContext app = (ApplicationContext)this.getApplication();
		SQLiteDatabase db = app.getDBHelper().getReadableDatabase();
		
		Cursor cursor = db.query(AssessmentDB.TABLE_NAME, null, null, null, null, null, null);		
		this.startManagingCursor(cursor);
		
		String[] from = new String[]{AssessmentDB._ID, AssessmentDB.ASSESSMENT_NAME }; 
		int[] to = new int[]{ R.id.assessmentId, R.id.assessmentListName };
		
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.assessment_list_item_renderer, cursor, from, to);
		
		// the 2nd approach is less code but the first possibly allows greater flexibility...
		// see this post for more details: http://stackoverflow.com/questions/5573539/android-using-simplecursoradapter-to-set-colour-not-just-strings
		
		this.setListAdapter(adapter);			
		db.close();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		
		TextView idTextView = (TextView)v.findViewById(R.id.assessmentId);
		String assessmentId = idTextView.getText().toString();
		Intent hazardListIntent = new Intent( this, HazardList.class );
		hazardListIntent.putExtra(Preferences.LIST_ASSESSMENT_ID, assessmentId);
		this.startActivity( hazardListIntent );
		super.onListItemClick(l, v, position, id);
	}
}
