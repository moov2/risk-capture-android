package com.risk.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.risk.ApplicationContext;
import com.risk.R;
import com.risk.activity.Main;
import com.risk.data.HazardDB;

public class RiskUploadService extends Service {

	//private static final String SERVICE_URL = "http://riskcapture.staging.moov2.com/RiskCapture/Risk/UploadRisk";
	
	private NotificationManager notiManager;
	
	private Handler mHandler;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override 
	public void onCreate(){
		super.onCreate();
		
		Log.d(Main.TAG, "STARTED RISK UPLOAD SERVICE");
		
		//notiManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		
		//displayNotification( "Starting Risk upload Service");
		
		
		//this.mHandler.removeCallbacks(thr.run());
		//this.mHandler.postDelayed(thr, 2000);
		
	}
	
	class ServiceWorker implements Runnable
	{
		//private Handler mHandler = new Handler();
		
		public void run(){
			while(true){
				try{
					Thread.sleep(5000);
					mHandler.sendEmptyMessage(0);
				}catch( InterruptedException e){
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void onDestroy()
	{
		this.displayNotification("Stopping Risk upload service");
		super.onDestroy();
	}
	
	@Override
	public void onStart(Intent intent, int startId){
		super.onStart(intent, startId);
		
		this.checkUploadedHazards();
		
		this.mHandler = new Handler(){
			@Override 
			public void handleMessage(Message msg){
				super.handleMessage(msg);
				Log.d(Main.TAG, "LOOP CHECK DB HERE");
			}
		};
		
		//start thread
		Thread thr = new Thread(null, new ServiceWorker(), "RiskUploadService");
		thr.start();
	}
	
	private void checkUploadedHazards()
	{
		ApplicationContext app = (ApplicationContext)this.getApplicationContext();
		SQLiteDatabase db = app.getDBHelper().getReadableDatabase();
		
		Cursor cursor = db.query(HazardDB.TABLE_NAME, null, null, null, null, null, null);
		Log.d(Main.TAG, "CHECKED DB and there are " + cursor.getCount());
		
		cursor.close();
		
	}
	
	private void displayNotification(String msg)
	{
		Notification notification = new Notification(android.R.drawable.stat_sys_upload, msg, System.currentTimeMillis());
		
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, Main.class), 0);
		
		notification.setLatestEventInfo(this, "Upload Service", msg, contentIntent);
		
		this.notiManager.notify(R.id.app_notification_id, notification);
	}
	
	public void uploadData(){

		try {
			
//			ContentResolver cr = getContentResolver();
//			URI imageURI = Uri.fromFile("Pic.jpg");
//			
//			this.bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, arg1)
//
//			ByteArrayOutputStream stream = new ByteArrayOutputStream();
//			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//			byte[] byteArray = stream.toByteArray();
//
//			HttpClient httpClient = new DefaultHttpClient();
//			HttpPost post = new HttpPost( SERVICE_URL );
//
//			ByteArrayBody bab = new ByteArrayBody(byteArray, "test.jpg");
//
//			MultipartEntity entity = new MultipartEntity( HttpMultipartMode.BROWSER_COMPATIBLE);
//			entity.addPart("image", bab);
//
//			post.setEntity(entity);
//
//			HttpResponse response = httpClient.execute(post);
//
//			Log.d(RiskCapture.TAG, "post image");


		} catch (Exception e) {
			Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
			.show();
			Log.e("Camera", e.toString());
		}

	}


}
