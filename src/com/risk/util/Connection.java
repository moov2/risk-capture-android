package com.risk.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class Connection {
	public static boolean Available(Context ctx){
		ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(conMgr!=null){
			NetworkInfo info = conMgr.getActiveNetworkInfo();
			if(info == null) return false;
			if(!info.isConnected()) return false;
			if(!info.isAvailable()) return false;
			return true;
		}else{
			return false;
		}
	}
}
