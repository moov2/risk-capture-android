package com.risk.util;

import java.io.IOException;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class BitmapUtil {

	public static Bitmap Create(Bitmap bm, int width, int height) {
		
		Float originalWidth  = Float.valueOf(bm.getWidth());
		Float originalHeight = Float.valueOf(bm.getHeight());
		Float ratio = originalWidth/originalHeight;
		return Bitmap.createScaledBitmap(bm, (int)(height*ratio), height, false);
	}
	
	public static Bitmap GetFromMediaStore(ContentResolver cr, Uri imageUri)
	{
		Bitmap bm = null;
		try
		{
			bm = MediaStore.Images.Media.getBitmap(cr, imageUri);
		}
		catch( IOException e)
		{
			Log.e("Error", "Failed to get bitmap");
		}
		
		return bm;
	}
}
