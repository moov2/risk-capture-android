package com.risk.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.risk.R;

public class MainViewButtonsAdapter extends BaseAdapter {
    private Context mContext;

    public MainViewButtonsAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
    	View v;

    	if (convertView == null) {  // if it's not recycled, initialize some attributes
        	
    		// get view from custom xml layout
    		LayoutInflater li = (LayoutInflater)this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.home_icon_button, null);
            // grab the icon_text and set it's value
            TextView tv = (TextView)v.findViewById(R.id.icon_text);
            tv.setText(mButtonLabels[position]);
            // grab the image and set its value
            ImageView iv = (ImageView)v.findViewById(R.id.icon_image);
            iv.setImageResource(mThumbIds[position]);
        
    	} else {
            v = convertView;
        }

        return v;
    }

    // references to our images and labels
    private Integer[] mThumbIds = {
            R.drawable.new_hazard,
            R.drawable.info,
            R.drawable.hazard_list
    };
    
    private String[] mButtonLabels = {
    	"New Hazard",
    	"Help",
    	"Assessments"
    };
    
}