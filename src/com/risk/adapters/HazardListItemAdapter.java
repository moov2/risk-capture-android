package com.risk.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.risk.R;
import com.risk.data.HazardDB;

public class HazardListItemAdapter extends SimpleCursorAdapter {
	
	private Context context;
	
	public HazardListItemAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
			this.context = context;
		}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

    	if (v == null) {  // if it's not recycled, initialize some attributes
    		// get view from custom xml layout
    		LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.hazard_item_renderer, null);
    	}

    	Cursor cursor = this.getCursor();
    	cursor.moveToPosition(position);

        // Assessment Name
        TextView tv = (TextView)v.findViewById(R.id.assessmentName);
        tv.setText(cursor.getString(cursor.getColumnIndex(HazardDB.ASSESSMENT_NAME)));
        
        // 	Hazard Type
        tv = (TextView)v.findViewById(R.id.hazardNotes);
        tv.setText(cursor.getString(cursor.getColumnIndex(HazardDB.TYPE)));
        
        // grab the image and set its value
        ImageView iv = (ImageView)v.findViewById(R.id.hazardImage);
        byte[] thumbnail = cursor.getBlob(cursor.getColumnIndex(HazardDB.THUMBNAIL));
        iv.setImageBitmap(BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length));

        return v;
	}
}
