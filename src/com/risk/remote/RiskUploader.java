package com.risk.remote;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.risk.data.AssessmentDB;
import com.risk.data.HazardDB;
import com.risk.data.Preferences;

public class RiskUploader extends AsyncTask<Integer, Integer, Integer> {

	private final String SERVICE_BASE_URL = "http://riskcapture.staging.moov2.com/api";	
	private String SERVICE_HAZARDS_URL = SERVICE_BASE_URL + "/hazards";
	private String SERVICE_HAZARDS_IMAGE_URL = SERVICE_HAZARDS_URL + "/{id}/image";
	private String SERVICE_ASSESSMENTS_URL = SERVICE_BASE_URL + "/assessments";
	private int _percentProgress;
	
	private SQLiteDatabase _db;
	private String _userEmail;
	private boolean _sendEmails;

	public RiskUploader(SQLiteDatabase db, String email, boolean sendEmails) throws Exception {
		if (!db.isOpen()) {
			throw new Exception("db must be open when passed");
		}
		_db = db;
		_userEmail = email;
		_sendEmails = sendEmails;
	}

	@Override
	protected Integer doInBackground(Integer... params) {
		_percentProgress = 0;
		uploadAssessmentToService(params[0]);
		return 201;
	}

//	public boolean uploadHazardToService(int hazardId) {
//		JSONObject hazardJson = getHazard(hazardId);
//		return uploadJsonObject(SERVICE_HAZARDS_URL, hazardJson);
//	}

	private boolean uploadAssessmentToService(int assessmentId) {
		JSONObject assessmentJson = getAssessment(assessmentId);
		return uploadJsonObject(SERVICE_ASSESSMENTS_URL, assessmentJson);
	}

	private boolean uploadJsonObject(String url, JSONObject toUpload) {
		HttpClient httpClient = new DefaultHttpClient();

		try {
			HttpResponse response;
			HttpPost post;
			
			StringEntity entity = new StringEntity(toUpload.toString());
			
			post = new HttpPost(url);
			post.setEntity(entity);
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-type", "application/json");			
			
			response = httpClient.execute(post);
			
			incrementProgress(20);

			Log.d("HTTP HAZARD UPLOAD", String.valueOf(response.getStatusLine().getStatusCode()));
			
			setProgress(100);
			return true;
		} catch (Exception e) {
			Log.e("HTTP HAZARD UPLOAD", e.toString());
			return false;
		} finally {
			try {
				httpClient.getConnectionManager().shutdown();
			} catch (Exception ignore) {
			};
		}
		
	}

	private Cursor getAssessmentCursor(int assessmentId) {
		return _db.query(AssessmentDB.TABLE_NAME, null, AssessmentDB._ID + " = ? ",
				new String[] { String.valueOf(assessmentId) }, null, null, null);
	}
	
	private Cursor getHazardsForAssessmentCursor(int assessmentId){
		return _db.query(HazardDB.TABLE_NAME, null, AssessmentDB.ASSESSMENT_ID + " = ? ",
				new String[] { String.valueOf(assessmentId) }, null, null, null);
	}

	private JSONObject getHazardJSON(Cursor cursor) {
		JSONObject output = new JSONObject();
		try {
			TimeZone tz = TimeZone.getTimeZone("UTC");
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		    df.setTimeZone(tz);
			output.put(HazardDB.REMOTE_ID,
					cursor.getString(cursor.getColumnIndex(HazardDB.REMOTE_ID)));
			output.put(HazardDB.ASSESSMENT_REMOTE_ID, cursor.getString(cursor
					.getColumnIndex(HazardDB.ASSESSMENT_REMOTE_ID)));
			output.put(HazardDB.NOTE,
					cursor.getString(cursor.getColumnIndex(HazardDB.NOTE)));
			output.put(HazardDB.LIKELIHOOD,
					cursor.getInt(cursor.getColumnIndex(HazardDB.LIKELIHOOD)));
			output.put(HazardDB.SEVERITY,
					cursor.getInt(cursor.getColumnIndex(HazardDB.SEVERITY)));
			output.put(HazardDB.TYPE,
					cursor.getInt(cursor.getColumnIndex(HazardDB.TYPE)));
			output.put(HazardDB.CREATED_DATE, df.format(new Date(cursor.getLong(cursor.getColumnIndex(HazardDB.CREATED_DATE)))));
			output.put(HazardDB.MODIFIED_DATE, df.format(new Date(cursor.getLong(cursor.getColumnIndex(HazardDB.MODIFIED_DATE)))));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return output;
	}

	private JSONObject getAssessment(int assessmentId) {
		JSONObject output = new JSONObject();
		Cursor assessmentCursor = getAssessmentCursor(assessmentId);
		Cursor assessmentHazardsCursor = getHazardsForAssessmentCursor(assessmentId);
		int hazardCount = assessmentHazardsCursor.getCount(); 
		if (assessmentCursor.getCount() > 0) {
			assessmentCursor.moveToFirst();
			output = getAssessmentJSON(assessmentCursor);
		}
		incrementProgress(5);
		onProgressUpdate(_percentProgress);
		if(hazardCount>0){
			JSONArray hazards = new JSONArray();
			assessmentHazardsCursor.moveToFirst();
			while(!assessmentHazardsCursor.isAfterLast()){
				hazards.put(getHazardJSON(assessmentHazardsCursor));
				incrementProgress((15/hazardCount));
				assessmentHazardsCursor.moveToNext();
			}
			try {
				output.put("hazards", hazards);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			uploadImages(assessmentHazardsCursor);
		}else{
			incrementProgress(75);
		}
		assessmentCursor.close();
		assessmentHazardsCursor.close();
		return output;
	}
	
	private void incrementProgress(int progress){
		_percentProgress += progress;
		onProgressUpdate(_percentProgress);
	}
	
	private void setProgress(int progress){
		_percentProgress = progress;
		onProgressUpdate(_percentProgress);
	}
	
	private void uploadImages(Cursor cursor) {
		HttpClient httpClient = new DefaultHttpClient(); 
		HttpResponse response;
		HttpPost post;
		MultipartEntity entity;
		int hazardCount = cursor.getCount();
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			try {
				String imagePath = cursor.getString(cursor.getColumnIndex(HazardDB.IMAGE_PATH));
				if(imagePath!=null && imagePath!="" && !imagePath.equals("")){ 
					File file = new File(imagePath);
					
					FileBody fileBody = new FileBody(file);
		
					entity = new MultipartEntity( HttpMultipartMode.BROWSER_COMPATIBLE);
					entity.addPart("image", fileBody); 
					
					post = new HttpPost( SERVICE_HAZARDS_IMAGE_URL.replace("{id}", cursor.getString(cursor.getColumnIndex(HazardDB.REMOTE_ID))) );
					post.setEntity(entity);
										
					response = httpClient.execute(post);
										
					Log.d("FileUploader Image Upload", "Uploaded image status code " + response.getStatusLine().getStatusCode());
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cursor.moveToNext();
			incrementProgress(60/hazardCount);
		}
	}

	private JSONObject getAssessmentJSON(Cursor cursor) {
		JSONObject output = new JSONObject();
		try {
			TimeZone tz = TimeZone.getTimeZone("UTC");
		    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		    df.setTimeZone(tz);
			output.put(AssessmentDB.REMOTE_ID,
					cursor.getString(cursor.getColumnIndex(AssessmentDB.REMOTE_ID)));
			output.put(AssessmentDB.ASSESSMENT_NAME,
					cursor.getString(cursor.getColumnIndex(AssessmentDB.ASSESSMENT_NAME)));
			output.put(AssessmentDB.CREATED_DATE,
					df.format(new Date(cursor.getLong(cursor.getColumnIndex(AssessmentDB.CREATED_DATE)))));
			output.put(AssessmentDB.MODIFIED_DATE,
					df.format(new Date(cursor.getLong(cursor.getColumnIndex(AssessmentDB.MODIFIED_DATE)))));
			output.put(Preferences.USER_EMAIL, _userEmail);
			output.put(Preferences.EMAIL_ASSESSMENTS, _sendEmails);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return output;
	}

	public void dispose() {
		if (_db.isOpen()) {
			_db.close();
		}
	}
}
